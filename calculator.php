<?php
$operand1 = $_GET["operand1"];
$operand2 = $_GET["operand2"];
$operator = $_GET["operator"];

if( !preg_match('/^[\d]+$/', $operand1) || !preg_match('/^[\d]+$/', $operand2) ){
	echo "One of the operands is NaN";
	exit;
}

switch($operator)
{
	case "addition":
		echo "Result of $operand1 + $operand2: " .($operand1 + $operand2);
		break;
	case "subtract":
		echo "Result of $operand1 - $operand2: " .($operand1 - $operand2);
		break;
	case "multiply":
		echo "Result of $operand1 * $operand2: " .($operand1 * $operand2);
		break;
	case "divide":
		if($operand2 == 0)
		{
			echo "Cannot divide by 0";
		}
		else
		{
			echo "Result of $operand1 / $operand2: " .($operand1 / $operand2);
		}
		break;
}

?>